﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Day25
{
	public class TvmTape: IList<byte>
	{
		private const int ChunkSize = 4*1024;
		private List<byte[]> Positive = new List<byte[]>{new byte[ChunkSize]};
		private List<byte[]> Negative = new List<byte[]>{new byte[ChunkSize]};
		public int LowerIdxBound { get; private set; }
		public int UpperIdxBound { get; private set; }

		public void Clear()
		{
			Positive = new List<byte[]>{ new byte[ChunkSize] };
			Negative = new List<byte[]>{ new byte[ChunkSize] };
			LowerIdxBound = UpperIdxBound = 0;
		}

		public int Count => UpperIdxBound - LowerIdxBound + 1;
		public bool IsReadOnly => false;
		public IEnumerator<byte> GetEnumerator() => new TvmTapeEnumerator(Negative, Positive, ChunkSize, LowerIdxBound, UpperIdxBound);
		IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

		private bool CheckConstraints(int index)
		{
			if (index < LowerIdxBound)
			{
				if (LowerIdxBound - index > 1)
					throw new InvalidOperationException("New lower bound is larger by more than 1 cell");

				LowerIdxBound = index;
				var absIdx = -LowerIdxBound - 1;
				var bankId = absIdx / ChunkSize;
				if (Negative.Count == bankId)
					Negative.Add(new byte[ChunkSize]);
				return true;
			}
			if (index > UpperIdxBound)
			{
				if (UpperIdxBound - index > 1)
					throw new InvalidOperationException("New upper bound is larger by more than 1 cell");

				UpperIdxBound = index;
				var bankId = index / ChunkSize;
				if (Positive.Count == bankId)
					Positive.Add(new byte[ChunkSize]);
				return true;
			}
			return false;
		}

		public byte this[int index]
		{
			get
			{
				if (CheckConstraints(index))
					return 0;

				if (index < 0)
				{
					var absIdx = -index - 1;
					return Negative[absIdx / ChunkSize][absIdx % ChunkSize];
				}
				return Positive[index / ChunkSize][index % ChunkSize];
			}
			set
			{
				CheckConstraints(index);
				if (index < 0)
				{
					var absIdx = -index - 1;
					Negative[absIdx / ChunkSize][absIdx % ChunkSize] = value;
				}
				else
					Positive[index / ChunkSize][index % ChunkSize] = value;
			}
		}

		public void Add(byte item) => throw new NotImplementedException();
		public bool Contains(byte item) => throw new NotImplementedException();
		public void CopyTo(byte[] array, int arrayIndex) => throw new NotImplementedException();
		public bool Remove(byte item) => throw new NotImplementedException();
		public int IndexOf(byte item) => throw new NotImplementedException();
		public void Insert(int index, byte item) => throw new NotImplementedException();
		public void RemoveAt(int index) => throw new NotImplementedException();

		private class TvmTapeEnumerator: IEnumerator<byte>
		{
			private readonly List<byte[]> negative;
			private readonly List<byte[]> positive;
			private readonly int chunkSize;
			private readonly int lowerIdx;
			private readonly int upperIdx;
			private int curIdx;

			public TvmTapeEnumerator(List<byte[]> negative, List<byte[]> positive, int chunkSize, int lowerIdx, int upperIdx)
			{
				this.negative = negative;
				this.positive = positive;
				this.chunkSize = chunkSize;
				this.lowerIdx = lowerIdx;
				this.upperIdx = upperIdx;
				this.curIdx = lowerIdx-1;
			}

			public void Dispose() {}

			public bool MoveNext()
			{
				if (curIdx > upperIdx)
					return false;

				curIdx++;
				return true;
			}

			public void Reset()
			{
				curIdx = lowerIdx-1;
			}

			public byte Current
			{
				get
				{
					if (curIdx < 0)
					{
						var absIdx = -curIdx-1;
						return negative[absIdx / chunkSize][absIdx % chunkSize];
					}
					return positive[curIdx / chunkSize][curIdx % chunkSize];
				}
			}

			object IEnumerator.Current => Current;
		}
	}
}