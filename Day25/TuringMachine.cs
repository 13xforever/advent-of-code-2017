﻿namespace Day25
{
	public static class TuringMachine
	{
		public static int GetChecksum(string input)
		{
			var vm = Tvm.Parse(input);
			for (var i = 0; i < vm.ChecksumTiming; i++)
				vm.Step();

			var csum = 0;
			foreach (var cell in vm.State.Tape)
				csum += cell;
			return csum;
		}
	}
}