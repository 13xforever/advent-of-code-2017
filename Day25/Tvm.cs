﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Day25
{
	using StateMachineAction = Action<TvmState>;
	using StateMachineEntry = Dictionary<byte, List<Action<TvmState>>>;
	using StateMachine = Dictionary<string, Dictionary<byte, List<Action<TvmState>>>>;

	public class Tvm
	{
		private static readonly char[] ValueSeparator = {' ', '\t', ':', '.'};
		public TvmState State { get; private set; }
		public readonly int ChecksumTiming;

		private Tvm(TvmState state, int checksumTiming)
		{
			State = state;
			ChecksumTiming = checksumTiming;
		}

		public void Step()
		{
			foreach (var f in State.Sm[State.CurrentState][State.Tape[State.IP]])
				f(State);
		}

		public override string ToString()
		{
			var r = new StringBuilder(State.Steps).Append(' ').Append(State.CurrentState).Append(": ");
			for (var i = Math.Min(State.Tape.LowerIdxBound, State.IP); i <= Math.Max(State.Tape.UpperIdxBound, State.IP); i++)
			{
				if (i == State.IP)
					r.Append('[').Append(State.Tape[i]).Append(']');
				else
					r.Append(State.Tape[i]);
			}
			return r.ToString();
		}

		public static Tvm Parse(string input)
		{
			string ParseState(string stateLine) => stateLine.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries)[3];
			int ParseChecksumTiming(string checksumLine) => int.Parse(checksumLine.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries)[5]);
			string ParseStateId(string[] stateLine) => stateLine[2];
			byte ParseEntryId(string[] stateLine) => byte.Parse(stateLine[5]);
			byte ParseWriteValue(string[] stateLine) => byte.Parse(stateLine[4]);
			int ParseDirection(string[] stateLine) => stateLine[6] == "left" ? -1 : 1;
			string ParseNextStateId(string[] stateLine) => stateLine[4];

			using (var reader = new StringReader(input))
			{
				var stateLine = reader.ReadLine();
				if (string.IsNullOrEmpty(stateLine))
					stateLine = reader.ReadLine();

				if (!stateLine.StartsWith("Begin"))
					throw new FormatException(stateLine);

				var startState = ParseState(stateLine);
				var checksumTimingLine = reader.ReadLine();
				if (!checksumTimingLine.StartsWith("Perform"))
					throw new FormatException(stateLine);

				var checksumTiming = ParseChecksumTiming(checksumTimingLine);
				string stateId = null;
				byte entryId = 255;
				var state = new TvmState{CurrentState = startState};
				string l;
				while ((l = reader.ReadLine()) != null)
				{
					if (string.IsNullOrEmpty(l))
						continue;

					var lp = l.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries);
					switch (lp[0])
					{
						case "In":
						{
							stateId = ParseStateId(lp);
							if (!state.Sm.ContainsKey(stateId))
								state.Sm[stateId] = new StateMachineEntry(2);
							break;
						}
						case "If":
						{
							entryId = ParseEntryId(lp);
							if (!state.Sm[stateId].ContainsKey(entryId))
								state.Sm[stateId][entryId] = new List<StateMachineAction>(3);
							break;
						}
						case "-":
						{
							switch (lp[1])
							{
								case "Write":
								{
									var val = ParseWriteValue(lp);
									state.Sm[stateId][entryId].Add(s => { s.Tape[s.IP] = val; });
									break;
								}
								case "Move":
								{
									var offset = ParseDirection(lp);
									state.Sm[stateId][entryId].Add(s => { s.IP += offset; });
									break;
								}
								case "Continue":
								{
									var nextId = ParseNextStateId(lp);
									state.Sm[stateId][entryId].Add(s => { s.CurrentState = nextId; });
									state.Steps++;
									break;
								}
								default:
									throw new InvalidOperationException($"Unknown instruction '{lp[1]}': {l}");
							}
							break;
						}
						default:
							throw new InvalidOperationException($"Unknown instruction '{lp[0]}': {l}");
					}
				}
				return new Tvm(state, checksumTiming);
			}
		}
	}

	public class TvmState
	{
		public StateMachine Sm { get; } = new StateMachine();
		public int IP;
		public int Steps;
		public string CurrentState;
		public TvmTape Tape { get; } = new TvmTape();
	}
}