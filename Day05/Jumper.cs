﻿using System;
using System.Linq;

namespace Day05
{
    public static class Jumper
	{
		private static readonly char[] separator = {'\r', '\n', '\t', ' '};

		public static int StepCount(string input)
		{
			var jumps = input.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
			var count = 0;
			var idx = 0;
			do
			{
				var offset = jumps[idx];
				jumps[idx]++;
				idx += offset;
				count++;
			} while (idx >= 0 && idx < jumps.Length);
			return count;
		}

		public static int StepCount2(string input)
		{
			var jumps = input.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
			var count = 0;
			var idx = 0;
			do
			{
				var offset = jumps[idx];
				if (offset > 2)
					jumps[idx]--;
				else
					jumps[idx]++;
				idx += offset;
				count++;
			} while (idx >= 0 && idx < jumps.Length);
			return count;
		}
    }
}
