﻿using NUnit.Framework;

namespace Day23
{
	[TestFixture]
	public class Tests23
	{
		[TestCase("set a 10", ExpectedResult = 0)]
		[TestCase("mul a 10", ExpectedResult = 1)]
		[TestCase(TestInput, ExpectedResult = 10)]
		[TestCase(RealInput, ExpectedResult = 6241)]
		public int Part1(string input) => CoProc.GetMulCount(input);

		[TestCase(OptimizedInput, ExpectedResult = 909)]
		public long Part2(string input) => CoProc.GetDebugResult(input);

		[TestCase(ExpectedResult = 909)]
		public long Part2Recompiled() => CoProc.DecompiledInput();

		private const string TestInput = @"
set a 10
mul b a
sub a 1
jnz a -2
";

		private const string RealInput = @"
set b 81
set c b
jnz a 2
jnz 1 5
mul b 100
sub b -100000
set c b
sub c -17000
set f 1
set d 2
set e 2
set g d
mul g e
sub g b
jnz g 2
set f 0
sub e -1
set g e
sub g b
jnz g -8
sub d -1
set g d
sub g b
jnz g -13
jnz f 2
sub h -1
set g b
sub g c
jnz g 2
jnz 1 3
sub b -17
jnz 1 -23
";

		private const string OptimizedInput = @"
set b 108100
set d 1
add d 1
set a b
mod a d
jnz a 3
add h 1
jnz 1 5
set a d
add a 1
sub a b
jnz a -9
set c b
sub c 125100
jnz c 2
jnz 1 3
add b 17
jnz 1 -16
";
	}
}
