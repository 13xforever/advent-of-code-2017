﻿using Day18;

namespace Day23
{
	public static class CoProc
	{
		public static int GetMulCount(string input)
		{
			var vm = new DuetVM(input, 0, false);
			do
			{
				vm.Step();
			} while (!vm.Stuck);
			return vm.MulCount;
		}

		public static long GetDebugResult(string input)
		{
			var vm = new DuetVM(input, 0, false);
			vm.Registers['a'] = 1;
			do
			{
				vm.Step();
			} while (!vm.Stuck);
			return vm.Registers['h'];
		}

		public static long DecompiledInput()
		{
			var h = 0;
			for (var b = 108100; b <= 125100; b += 17)
			for (var d = 2; d < b; d++)
				if (b % d == 0)
				{
					h++;
					break;
				}
			return h;
		}
	}
}