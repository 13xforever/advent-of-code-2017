set b 108100
set c 125100

.l3:
set f 1
set d 2

.l2:
set e 2

.l1:
set g d
mul g e
sub g b
jnz g .f1
set f 0

.f1:
add e 1
set g e
sub g b
jnz g .l1
add d 1
set g d
sub g b
jnz g .l2
jnz f .f2
add h 1

.f2:
set g b
sub g c
jnz g .f3
jnz 1 .exit

.f3:
add b 17
jnz 1 .l3

.exit: