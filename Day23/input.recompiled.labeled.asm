set b 108100

.l1:
set d 1

.l2:
add d 1
set a b
mod a d
jnz a .l2c
add h 1
jnz 1 .l2e
.l2c:
set a d
add a 1
sub a b
jnz a .l2

.l2e:
set c b
sub c 125100
jnz c .l1c
jnz 1 .exit
.l1c:
add b 17
jnz 1 .l1

.exit
