﻿using System;

namespace Day11
{
    public static class HexGrid
	{
		private static readonly char[] separator = {',', ' ', '\t'};

		public static int GetOptimizedPathLength(string path)
		{
			return TracePathLength(path).finalLength;
		}

		public static int GetMaximumDistance(string path)
		{
			return TracePathLength(path).maxLength;
		}
		
		private static (int maxLength, int finalLength) TracePathLength(string path)
		{
			var steps = path.Split(separator, StringSplitOptions.RemoveEmptyEntries);
			var (x, y) = (0, 0);
			var maxLength = 0;
			int currentPathLength = 0;
			foreach (var step in steps)
			{
				switch (step)
				{
					case "n":
						y += 2;
						break;
					case "s":
						y -= 2;
						break;
					case "nw":
						(x, y) = (x - 1, y + 1);
						break;
					case "ne":
						(x, y) = (x + 1, y + 1);
						break;
					case "sw":
						(x, y) = (x - 1, y - 1);
						break;
					case "se":
						(x, y) = (x + 1, y - 1);
						break;
					default:
						throw new InvalidOperationException($"Unknown direction <{step}>");
				}
				currentPathLength = GetLength(x, y);
				if (currentPathLength > maxLength)
					maxLength = currentPathLength;
			}
			return (maxLength, currentPathLength);
		}

		private static int GetLength(int x, int y)
		{
			(x, y) = (Math.Abs(x), Math.Abs(y));
			if (x == 0 && y == 0)
				return 0;

			var diff = Math.Abs(x - y);
			return Math.Min(x, y) + diff / 2;
		}
    }
}
