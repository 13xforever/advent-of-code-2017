﻿using NUnit.Framework;

namespace Day13
{
	[TestFixture]
	public class Tests13
	{
		[TestCase(TestInput, ExpectedResult = 24)]
		[TestCase(RealInput, ExpectedResult = 1840)]
		public int Part1(string input) => PacketScanner.GetSeverity(input);

		[TestCase(TestInput, ExpectedResult = 10)]
		[TestCase(RealInput, ExpectedResult = 3850260)]
		public int Part2(string input) => PacketScanner.GetShortestDelay(input);

		private const string TestInput =
			@"0: 3
			  1: 2
			  4: 4
			  6: 4";

		private const string RealInput =
			@"0: 5
1: 2
2: 3
4: 4
6: 6
8: 4
10: 8
12: 6
14: 6
16: 8
18: 6
20: 9
22: 8
24: 10
26: 8
28: 8
30: 12
32: 8
34: 12
36: 10
38: 12
40: 12
42: 12
44: 12
46: 12
48: 14
50: 12
52: 14
54: 12
56: 14
58: 12
60: 14
62: 14
64: 14
66: 14
68: 14
70: 14
72: 14
76: 14
80: 18
84: 14
90: 18
92: 17";
	}
}