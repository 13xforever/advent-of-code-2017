﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day13
{
	using Firewall = List<(int layer, int depth)>;

    public static class PacketScanner
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};
		private static readonly char[] ValueSeparator = {'\t', ' ', ':'};

		public static int GetSeverity(string firewallLayout)
		{
			var firewall = Parse(firewallLayout);
			return GetSeverity(firewall, 0).severity;
		}

		public static int GetShortestDelay(string firewallLayout)
		{
			var firewall = Parse(firewallLayout);
			for (var delay = 0; delay < int.MaxValue ; delay++)
				if (!GetSeverity(firewall, delay).caught)
					return delay;

			throw new TimeoutException("You done goofed this one, boi");
		}

		private static (bool caught, int severity) GetSeverity(Firewall firewall, int delay)
		{
			var result = 0;
			var caught = false;
			foreach (var layer in firewall)
			{
				var scanPathLength = (layer.depth - 1) * 2;
				if ((delay + layer.layer) % scanPathLength == 0)
				{
					caught = true;
					result += layer.layer * layer.depth;
				}
			}
			return (caught, result);
		}

		private static Firewall Parse(string firewallLayout)
		{
			return firewallLayout
				.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries)
				.Select(l => l.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray())
				.Select(a => (layer: a[0], depth: a[1]))
				.ToList();
		}

    }
}
