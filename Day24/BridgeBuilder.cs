﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day24
{
	using PartsInventory = Dictionary<int, Dictionary<int, int>>;

	public static class BridgeBuilder
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};
		private static readonly char[] ValueSeparator = {' ', '\t', '/'};

		public static int GetStrongestBridgeValue(string input)
		{
			var parts = Parse(input);
			return BuildStrongestBridge(parts, (c, m) => true).strength;
		}

		public static int GetStrongestOfLongestBridgeValue(string input)
		{
			var parts = Parse(input);
			return BuildStrongestBridge(parts, (c, m) => c >= m).strength;
		}

		private static (string bridge, int strength, int length) BuildStrongestBridge(PartsInventory parts, Func<int, int, bool> acceptableLength)
		{
			var br = "";
			var maxStrength = 0;
			var maxLength = 0;
			var startingParts = parts[0];
			foreach (var outPins in startingParts.Keys)
			{
				var (b, s, l) = BuildBridge(outPins, outPins, 1, acceptableLength, "".AppendBridge(0, outPins), parts.Without(0, outPins));
				if (acceptableLength(l, maxLength) && s > maxStrength)
				{
					br = b;
					maxStrength = s;
					maxLength = l;
				}
			}
			return (br, maxStrength, maxLength);
		}

		private static (string bridge, int strength, int length) BuildBridge(int inPins, int currentStrength, int currentLength, Func<int, int, bool> acceptableLength, string currentBridge, PartsInventory parts)
		{
			if (!parts.TryGetValue(inPins, out var compatibleParts) || compatibleParts.Count == 0)
				return (currentBridge, currentStrength, currentLength);

			var br = currentBridge;
			var maxStrength = currentStrength = currentStrength + inPins;
			var maxLength = currentLength = currentLength + 1;
			foreach (var outPins in compatibleParts.Keys)
			{
				var (b, s, l) = BuildBridge(outPins, currentStrength + outPins, currentLength, acceptableLength, currentBridge.AppendBridge(inPins, outPins), parts.Without(inPins, outPins));
				if (acceptableLength(l, maxLength) && s > maxStrength)
				{
					br = b;
					maxStrength = s;
					maxLength = l;
				}
			}
			return (br, maxStrength, maxLength);
		}

		private static PartsInventory Without(this PartsInventory inventory, int @in, int @out)
		{
			var result = new PartsInventory(inventory.Count);
			foreach (var keyIn in inventory.Keys)
			{
				var entry = new Dictionary<int, int>();
				foreach (var keyOut in inventory[keyIn].Keys)
				{
					if ((keyIn == @in && keyOut == @out) || (keyIn == @out && keyOut == @in))
					{
						if (keyIn == keyOut)
						{
							if (inventory[keyIn][keyOut] == 2)
								continue;

							entry[keyOut] = inventory[keyIn][keyOut] - 2;
						}
						else
						{
							if (inventory[keyIn][keyOut] == 1)
								continue;
							entry[keyOut] = inventory[keyIn][keyOut] - 1;
						}
					}
					else
						entry[keyOut] = inventory[keyIn][keyOut];
				}
				if (entry.Count > 0)
					result[keyIn] = entry;
			}
			return result;
		}

		private static string AppendBridge(this string s, int @in, int @out) => $"{s}{@in}/{@out}→";

		private static PartsInventory Parse(string input)
		{
			var result = new PartsInventory();
			var parts = input.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries)
				.Select(l => l.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray());
			foreach (var part in parts)
			{
				if (!result.TryGetValue(part[0], out var sp))
					result[part[0]] = sp = new Dictionary<int, int>();
				if (sp.ContainsKey(part[1]))
					sp[part[1]]++;
				else
					sp[part[1]] = 1;

				if (!result.TryGetValue(part[1], out sp))
					result[part[1]] = sp = new Dictionary<int, int>();
				if (sp.ContainsKey(part[0]))
					sp[part[0]]++;
				else
					sp[part[0]] = 1;
			}
			return result;
		}
		
	}
}