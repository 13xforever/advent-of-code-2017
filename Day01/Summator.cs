﻿using System;

namespace Day01
{
    public static class Summator
    {
		private static bool IsDigit(char c) => c >= '0' && c <= '9';
		private static int ToDigit(char c) => c - '0';

		public static int Sum(string input, int offset)
		{
			if (input.Length < 2)
				return 0;

			var result = 0;
			for (var i=0; i<input.Length; i++)
			{
				var c = input[i];
				var newc = input[(i+offset)%input.Length];
				if (!IsDigit(newc))
					throw new InvalidOperationException($"{newc} is not a digit");

				if (newc == c)
					result += ToDigit(c);
			}
			return result;
		}
    }
}
