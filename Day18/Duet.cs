﻿using System.Linq;

namespace Day18
{
	public static class Duet
	{
		public static long GetFirstRcvFreq(string input)
		{
			var vm = new DuetVM(input, 0, false);
			do
			{
				vm.Step();
			} while (vm.RcvFreq == null);
			return vm.RcvQueue.Count > 0 ? vm.RcvQueue.Last() : vm.RcvFreq.Value;
		}

		public static int CountSnds(string input)
		{
			var vm0 = new DuetVM(input, 0, true);
			var vm1 = new DuetVM(input, 1, true);
			vm0.SendTo(vm1);
			vm1.SendTo(vm0);
			do
			{
				if (!vm0.Stuck) vm0.Step();
				if (!vm1.Stuck) vm1.Step();
			} while (!(vm0.Stuck && vm1.Stuck));
			return vm1.SndCount;
		}
	}
}