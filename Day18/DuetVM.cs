﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day18
{
	using Registers = Dictionary<char, long>;

	public class DuetVM
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};
		private static readonly char[] ValueSeparator = {' ', '\t'};
		private readonly Action[] JitCache;
		private readonly bool skipChkOnRcv;

		public int Id { get; }
		public readonly string[] Source;
		public readonly Registers Registers;
		public long IP { get; private set; } = 0;
		public string LastLine { get; private set; } = "";
		public Queue<long> SndQueue { get; private set; }
		public Queue<long> RcvQueue { get; private set; }
		public long? RcvFreq { get; private set; } = null;
		public int SndCount { get; private set; } = 0;
		public int MulCount { get; private set; } = 0;
		public bool Stuck => IP < 0 || IP >= JitCache.Length || (LastLine.StartsWith("rcv") && RcvQueue.Count == 0);

		public DuetVM(string instructions, int instanceId, bool skipChkOnRcv)
		{
			Id = instanceId;
			this.skipChkOnRcv = skipChkOnRcv;
			Source = instructions.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries);
			Registers = Enumerable.Range('a', 'z' - 'a').ToDictionary(i => (char)i, _ => 0L);
			Registers['p'] = instanceId;
			SndQueue = new Queue<long>();
			RcvQueue = SndQueue;
			JitCache = new Action[Source.Length];
			BuildJitCache();
		}

		public void SendTo(DuetVM receiver)
		{
			receiver.RcvQueue = SndQueue;
		}

		public void Step()
		{
			if (IP < 0 || IP >= JitCache.Length)
				throw new InvalidOperationException("Halted. Invalid IP value " + IP);
			JitCache[IP]();
		}

		private void BuildJitCache()
		{
			for (var i = 0; i < Source.Length; i++)
			{
				var srcLine = Source[i];
				var inst = srcLine.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries);
				switch (inst[0])
				{
					case "snd":
					{
						if (int.TryParse(inst[1], out var val))
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								SndQueue.Enqueue(val);
								SndCount++;
								IP++;
							};
						else
						{
							var reg = inst[1][0];
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								SndQueue.Enqueue(Registers[reg]);
								SndCount++;
								IP++;
							};

						}
						break;
					}
					case "set":
					{
						var dst = inst[1][0];
						if (int.TryParse(inst[2], out var val))
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] = val;
								IP++;
							};
						else
						{
							var src = inst[2][0];
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] = Registers[src];
								IP++;
							};
						}
						break;
					}
					case "add":
					{
						var dst = inst[1][0];
						if (int.TryParse(inst[2], out var val))
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] += val;
								IP++;
							};
						else
						{
							var src = inst[2][0];
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] += Registers[src];
								IP++;
							};
						}
						break;
					}
					case "sub":
					{
						var dst = inst[1][0];
						if (int.TryParse(inst[2], out var val))
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] -= val;
								IP++;
							};
						else
						{
							var src = inst[2][0];
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] -= Registers[src];
								IP++;
							};
						}
						break;
					}
					case "mul":
					{
						var dst = inst[1][0];
						if (int.TryParse(inst[2], out var val))
							JitCache[i] = () =>
							{
								MulCount++;
								LastLine = srcLine;
								Registers[dst] *= val;
								IP++;
							};
						else
						{
							var src = inst[2][0];
							JitCache[i] = () =>
							{
								MulCount++;
								LastLine = srcLine;
								Registers[dst] *= Registers[src];
								IP++;
							};
						}
						break;
					}
					case "mod":
					{
						var dst = inst[1][0];
						if (int.TryParse(inst[2], out var val))
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] %= val;
								IP++;
							};
						else
						{
							var src = inst[2][0];
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								Registers[dst] %= Registers[src];
								IP++;
							};
						}
						break;
					}
					case "rcv":
					{
						if (int.TryParse(inst[1], out var val))
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								if (skipChkOnRcv || val != 0)
								{
									if (RcvQueue.Count > 0)
									{
										RcvFreq = RcvQueue.Dequeue();
										IP++;
									}
								}
								else
									IP++;
							};
						else
						{
							var src = inst[1][0];
							JitCache[i] = () =>
							{
								LastLine = srcLine;
								if (skipChkOnRcv || Registers[src] != 0)
								{
									if (RcvQueue.Count > 0)
									{
										RcvFreq = RcvQueue.Dequeue();
										Registers[src] = RcvFreq.Value;
										IP++;
									}
								}
								else
									IP++;
							};
						}
						break;
					}
					case "jgz":
					{
						if (int.TryParse(inst[1], out var condVal))
						{
							if (int.TryParse(inst[2], out var offsetVal))
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (condVal > 0)
										IP += offsetVal;
									else
										IP++;
								};
							else
							{
								var src = inst[2][0];
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (condVal > 0)
										IP += Registers[src];
									else
										IP++;
								};
							}
						}
						else
						{
							var condReg = inst[1][0];
							if (int.TryParse(inst[2], out var offsetVal))
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (Registers[condReg] > 0)
										IP += offsetVal;
									else
										IP++;
								};
							else
							{
								var src = inst[2][0];
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (Registers[condReg] > 0)
										IP += Registers[src];
									else
										IP++;
								};
							}
						}
						break;
					}
					case "jnz":
					{
						if (int.TryParse(inst[1], out var condVal))
						{
							if (int.TryParse(inst[2], out var offsetVal))
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (condVal != 0)
										IP += offsetVal;
									else
										IP++;
								};
							else
							{
								var src = inst[2][0];
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (condVal != 0)
										IP += Registers[src];
									else
										IP++;
								};
							}
						}
						else
						{
							var condReg = inst[1][0];
							if (int.TryParse(inst[2], out var offsetVal))
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (Registers[condReg] != 0)
										IP += offsetVal;
									else
										IP++;
								};
							else
							{
								var src = inst[2][0];
								JitCache[i] = () =>
								{
									LastLine = srcLine;
									if (Registers[condReg] != 0)
										IP += Registers[src];
									else
										IP++;
								};
							}
						}
						break;
					}
					default:
						throw new InvalidOperationException("Invalid instruction " + Source[i]);
				}
			}
		}
	}
}