﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day21
{
	using Transform2 = Dictionary<Pattern, Pattern>;
	using Transform3 = Dictionary<Pattern, Pattern[,]>;

	public static class Fractal
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};
		private static readonly char[] ValueSeparator = {' ', '\t', '=', '>'};
		private static readonly string StartPattern = ".#./..#/###";

		public static int GetFinalCount(string patterns, int iterations)
		{
			var result = DoTransforms(patterns, iterations);
			var gridSize = result.GetLength(0);
			var onesCount = 0;
			for (var y = 0; y < gridSize; y++)
			for (var x = 0; x < gridSize; x++)
				onesCount += result[y,x].OnesCount;
			return onesCount;
		}

		private static Pattern[,] DoTransforms(string patterns, int iterations)
		{
			var result = new[,] {{Pattern.Parse(StartPattern)}}; //1x1
			var (t2, t3) = Parse(patterns);
			for (var i = 0; i < iterations; i++)
			{
				if (result[0, 0].Size == 3 && result.GetLength(0) % 2 == 0)
					result = Pattern.Recombine(result);
				result = Transform(result, t2, t3);
			}
			return result;
		}

		private static Pattern[,] Transform(Pattern[,] grid, Transform2 t2, Transform3 t3)
		{
			switch (grid[0, 0].Size)
			{
				case 2:
				{
					var gridSize = grid.GetLength(0);
					var result = new Pattern[gridSize, gridSize];
					for (var y = 0; y < gridSize; y++)
					for (var x = 0; x < gridSize; x++)
						result[y, x] = t2[grid[y, x]];
					return result;
				}
				case 3:
				{
					var gridSize = grid.GetLength(0);
					var result = new Pattern[gridSize*2, gridSize*2];
					for (var y = 0; y < gridSize; y++)
					for (var x = 0; x < gridSize; x++)
					{
						if (!t3.TryGetValue(grid[y, x], out var np))
							throw new InvalidOperationException("Missing pattern!");

						result[y * 2, x * 2] = np[0, 0];
						result[y * 2, x * 2 + 1] = np[0, 1];
						result[y * 2 + 1, x * 2] = np[1, 0];
						result[y * 2 + 1, x * 2 + 1] = np[1, 1];
					}
					return result;
				}
				default:
					throw new InvalidOperationException("How did you end up with the grid of patterns of size " + grid[0, 0].Size);
			}
		}

		private static (Transform2 t2, Transform3 t3) Parse(string patterns)
		{
			var result2 = new Transform2();
			var result3 = new Transform3();
			foreach (var l in patterns.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries))
			{
				var lp = l.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries).ToArray();
				if (lp.Length != 2)
					throw new FormatException("Invalid pattern format: " + l);

				var pf = Pattern.Parse(lp[0]);
				var pt = Pattern.Parse(lp[1]);
				switch (pf.Size)
				{
					case 2:
					{
						result2[pf] = pt;
						result2[pf.FlipH()] = pt;
						for (var i = 0; i < 3; i++)
						{
							pf = pf.Rotate();
							result2[pf] = pt;
							result2[pf.FlipH()] = pt;
						}
						break;
					}
					case 3:
					{
						var ptb = pt.Break();
						result3[pf] = ptb;
						result3[pf.FlipH()] = ptb;
						for (var i = 0; i < 3; i++)
						{
							pf = pf.Rotate();
							result3[pf] = ptb;
							result3[pf.FlipH()] = ptb;
						}
						break;
					}
					default:
						throw new InvalidOperationException("Invalid source pattern of size " + pf.Size);
				}
			}
			return (result2, result3);
		}
	}
}