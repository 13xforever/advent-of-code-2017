﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day21
{
	internal class Pattern
	{
		public readonly byte[][] Grid;
		public readonly byte Size;
		public readonly byte OnesCount;
		private readonly int hash;
		private static readonly char[] RowSeparator = {'/'};
		private static readonly Dictionary<char, byte> Converter = new Dictionary<char, byte>{{'.', 0}, {'#', 1},};
		private readonly string str;

		public Pattern(byte[][] grid, string str = null)
		{
			Grid = grid;
			Size = (byte)grid.Length;
			(hash, OnesCount) = CalcHash(grid);
			this.str = str ?? ToString(grid);
		}

		public Pattern Rotate()
		{
			var gridSize = Grid.Length;
			var result = new byte[gridSize][];
			for (var y = 0; y < gridSize; y++)
				result[y] = new byte[gridSize];
			for (var y = 0; y < gridSize; y++)
			for (var x = 0; x < gridSize; x++)
				result[x][gridSize - 1 - y] = Grid[y][x];
			return new Pattern(result);
		}

		public Pattern FlipH()
		{
			var gridSize = Grid.Length;
			var result = new byte[gridSize][];
			for (var y = 0; y < gridSize; y++)
			{
				result[y] = new byte[gridSize];
				for (var x = 0; x < gridSize; x++)
					result[y][x] = Grid[y][gridSize - 1 - x];
			}
			return new Pattern(result);
		}

		public Pattern FlipV()
		{
			var gridSize = Grid.Length;
			var result = new byte[gridSize][];
			for (var y = 0; y < gridSize; y++)
			{
				result[y] = new byte[gridSize];
				for (var x = 0; x < gridSize; x++)
					result[y][x] = Grid[gridSize - 1 - y][x];
			}
			return new Pattern(result);
		}

		public Pattern[,] Break()
		{
			if (Size != 4)
				throw new InvalidOperationException("Can't break up pattern of size " + Size);

			var result = new Pattern[2, 2];
			for (var y = 0; y < 2; y++)
			for (var x = 0; x < 2; x++)
			{
				var sp = new byte[2][];
				for (var spy = 0; spy < 2; spy++)
				{
					sp[spy] = new byte[2];
					for (var spx = 0; spx < 2; spx++)
						sp[spy][spx] = Grid[y * 2 + spy][x * 2 + spx];
				}
				result[y, x] = new Pattern(sp);
			}
			return result;
		}

		public static Pattern[,] Recombine(Pattern[,] macroPattern)
		{
			var macroSize = macroPattern.GetLength(0);
			if (macroSize % 2 == 1 || macroPattern[0, 0].Size != 3)
				throw new InvalidOperationException("Recombine only works on even number of 3x3 patterns");

			var tmp = new byte[macroSize * 3][];
			for (var i = 0; i < tmp.Length; i++)
				tmp[i] = new byte[tmp.Length];
			for (var my = 0; my < macroSize; my++)
			for (var mx = 0; mx < macroSize; mx++)
			for (var ly = 0; ly < 3; ly++)
			for (var lx = 0; lx < 3; lx++)
				tmp[my * 3 + ly][mx * 3 + lx] = macroPattern[my, mx].Grid[ly][lx];

			var newMacroSize = macroSize / 2 * 3;
			var result = new Pattern[newMacroSize, newMacroSize];
			for (var my = 0; my < newMacroSize; my++)
			for (var mx = 0; mx < newMacroSize; mx++)
			{
				var g = new byte[2][];
				for (var ly = 0; ly < 2; ly++)
				{
					g[ly] = new byte[2];
					for (var lx = 0; lx < 2; lx++)
						g[ly][lx] = tmp[my * 2 + ly][mx * 2 + lx];
				}
				result[my, mx] = new Pattern(g);
			}
			return result;
		}

		public static Pattern Parse(string pattern)
		{
			var cp = pattern.Split(RowSeparator)
				.Select(r => r.ToCharArray().Select(c => Converter[c]).ToArray())
				.ToArray();
			return new Pattern(cp, pattern);
		}

		private static string ToString(byte[][] grid)
		{
			return string.Join("/", grid.Select(r => string.Concat(r.Select(b => b == 0 ? '.' : '#'))));
		}

		private static (int h, byte oc) CalcHash(byte[][] grid)
		{
			var result = 0;
			var onesCount = 0;
			for (var y = 0; y < grid.Length; y++)
			for (var x = 0; x < grid.Length; x++)
			{
				result = (result << 1) | grid[y][x];
				onesCount += grid[y][x];
			}
			result |= (grid.Length << 16);
			return (result, (byte)onesCount);
		}

		public override string ToString() => str;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((Pattern)obj);
		}

		protected bool Equals(Pattern other) => hash == other.hash;
		public override int GetHashCode() => hash;
		public static bool operator ==(Pattern left, Pattern right) => Equals(left, right);
		public static bool operator !=(Pattern left, Pattern right) => !Equals(left, right);
	}
}