﻿using System.Collections.Generic;

namespace Day17
{
	public static class Spinlock
	{
		public static int GetNextValue(int step)
		{
			var result = new List<ushort>(2018) {0, 1};
			var idx = 1;
			for (ushort i = 2; i < 2018; i++)
			{
				idx = (idx + step + 1) % i;
				result.Insert(idx, i);
			}
			return result[(idx + 1) % 2018];
		}

		public static int GetValueAfter0(int step, int iterations)
		{
			var idx = 1;
			var zeroIdx = 0;
			var result = 1;
			for (var i = 2; i < iterations; i++)
			{
				idx = (idx + step + 1) % i;
				if (idx <= zeroIdx)
					zeroIdx = (zeroIdx + 1) % i;
				else if (idx == (zeroIdx + 1) % i)
					result = i;
			}
			return result;
		}
	}
}