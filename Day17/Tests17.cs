﻿using NUnit.Framework;

namespace Day17
{
	[TestFixture]
	public class Tests17
	{
		[TestCase(3, ExpectedResult = 638)]
		[TestCase(328, ExpectedResult = 1670)]
		public int Part1(int step) => Spinlock.GetNextValue(step);

		[TestCase(3, 2017, ExpectedResult = 1226)]
		[TestCase(328, 2017, ExpectedResult = 1574)]
		[TestCase(328, 50_000_000, ExpectedResult = 2316253)]
		public int Part2(int step, int iterations) => Spinlock.GetValueAfter0(step, iterations+1);
	}
}
