﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day08
{
    public static class VM
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};
		private static readonly char[] ValueSeparator = {' ', '\t'};

		public static int GetLargestResult(string input)
		{
			var result = Execute(input);
			return result.regs.Values.DefaultIfEmpty().Max();
		}

		public static int GetMaximumValue(string input)
		{
			var result = Execute(input);
			return result.max;
		}

		private static (Dictionary<string, int> regs, int max) Execute(string input)
		{
			var instructions = input.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries).Select(s => s.Trim().Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries)).ToList();
			var regs = new Dictionary<string, int>();
			var max = 0;
			foreach (var line in instructions)
			{
				if (line.Length != 7)
					throw new InvalidOperationException("Invalid instruction: " + string.Join(" ", line));

				if (line[3] != "if")
					throw new InvalidOperationException("Invalid instruction, if expected: " + string.Join(" ", line));

				var (dest, op, val, dep, conOp, conVal) = (line[0], line[1], int.Parse(line[2]), line[4], line[5], int.Parse(line[6]));
				if (!regs.TryGetValue(dep, out var depVal))
					regs[dep] = 0;
				switch (conOp)
				{
					case "==" when depVal == conVal:
					case "!=" when depVal != conVal:
					case ">=" when depVal >= conVal:
					case "<=" when depVal <= conVal:
					case ">" when depVal > conVal:
					case "<" when depVal < conVal:
						if (!regs.ContainsKey(dest))
							regs[dest] = 0;
						switch (op)
						{
							case "inc":
								regs[dest] += val;
								break;
							case "dec":
								regs[dest] -= val;
								break;
							default:
								throw new InvalidOperationException("Unknown operation: " + string.Join(" ", line));
						}
						if (max < regs[dest])
							max = regs[dest];
						break;
				}
			}
			return (regs, max);
		}
    }
}
