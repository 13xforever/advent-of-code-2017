﻿using System.Collections.Generic;
using System.Linq;

namespace Day15
{
	public static class Judge
	{
		public static int GetDiffRank(IEnumerable<int> genA, IEnumerable<int> genB, int iterations) => genA.Zip(genB, (a, b) => (a ^ b) & 0xffff).Take(iterations).Count(r => r == 0);
	}
}
