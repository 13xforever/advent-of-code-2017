﻿using System.Linq;
using NUnit.Framework;

namespace Day15
{
	[TestFixture]
	public class Tests15
	{
		[TestCase('A', 65, ExpectedResult = new[] {1092455, 1181022009, 245556042, 1744312007, 1352636452})]
		[TestCase('B', 8921, ExpectedResult = new[] {430625591, 1233683848, 1431495498, 137874439, 285222916})]
		public int[] GeneratorTest1(char type, int seed)
		{
			var gen = type == 'A' ? Generator.GetA1(seed) : Generator.GetB1(seed);
			return gen.Take(5).ToArray();
		}

		[TestCase('A', 65, ExpectedResult = new[] { 1352636452, 1992081072, 530830436, 1980017072, 740335192 })]
		[TestCase('B', 8921, ExpectedResult = new[] { 1233683848, 862516352, 1159784568, 1616057672, 412269392 })]
		public int[] GeneratorTest2(char type, int seed)
		{
			var gen = type == 'A' ? Generator.GetA2(seed) : Generator.GetB2(seed);
			return gen.Take(5).ToArray();
		}

		[TestCase(65, 8921, ExpectedResult = 588)]
		[TestCase(679, 771, ExpectedResult = 626)]
		public int Part1(int seedA, int seedB) => Judge.GetDiffRank(Generator.GetA1(seedA), Generator.GetB1(seedB), 40_000_000);

		[TestCase(65, 8921, ExpectedResult = 309)]
		[TestCase(679, 771, ExpectedResult = 306)]
		public int Part2(int seedA, int seedB) => Judge.GetDiffRank(Generator.GetA2(seedA), Generator.GetB2(seedB), 5_000_000);
	}
}