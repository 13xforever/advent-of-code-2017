﻿using System;
using System.Collections.Generic;

namespace Day15
{
	internal class Generator
	{
		private readonly int seed;
		private readonly int factor;

		private Generator(int seed, int factor)
		{
			this.seed = seed;
			this.factor = factor;
		}

		private IEnumerable<int> Next(Func<long, bool> filter)
		{
			long result = seed;
			while (true)
			{
				result = (result * factor) % 2147483647;
				if (filter(result))
					yield return (int)result; // 0x7fffffff
			}
		}

		public static IEnumerable<int> GetA1(int seed) => new Generator(seed, 16807).Next(i => true); //0x41a7
		public static IEnumerable<int> GetB1(int seed) => new Generator(seed, 48271).Next(i => true); //0xbc8f
		public static IEnumerable<int> GetA2(int seed) => new Generator(seed, 16807).Next(i => i%4==0); //0x41a7
		public static IEnumerable<int> GetB2(int seed) => new Generator(seed, 48271).Next(i => i%8==0); //0xbc8f
	}
}