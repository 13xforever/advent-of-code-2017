﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day02
{
    public static class Checksum
    {
		private static readonly char[] space = {' ', '\t'};
		private static readonly char[] newLine = {'\r', '\n'};

		public static int Calc(string table, Func<IList<int>, int> checksum)
		{
			return table.Split(newLine, StringSplitOptions.RemoveEmptyEntries).Select(ReadNumbersInARow).Select(checksum).Sum();
		}

		public static int MinMax(IList<int> sequence)
		{
			int? min = null, max = null;
			foreach (var n in sequence)
			{
				if (min == null || min > n)
					min = n;
				if (max == null || max < n)
					max = n;
			}
			return (max ?? 0) - (min ?? 0);
		}

		public static int Div(IList<int> sequence)
		{
			for (var i = 0; i < sequence.Count-1; i++)
			for (var j = i + 1; j < sequence.Count; j++)
			{
				if (sequence[i] % sequence[j] == 0)
					return sequence[i] / sequence[j];
				if (sequence[j] % sequence[i] == 0)
					return sequence[j] / sequence[i];
			}
			return 0;
		}

		private static List<int> ReadNumbersInARow(string row)
		{
			return row.Split(space, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();
		}
    }
}
