﻿using System;

namespace Day09
{
    public static class Processor
	{
		public static int GetScore(string stream)
		{
			return Process(stream).Score;
		}

		public static int GetGarbageCount(string stream)
		{
			return Process(stream).GarbageCount;
		}

		private static State Process(string stream)
		{
			var state = new State { Process = GroupParser };
			foreach (var c in stream)
				state = state.Process(c, state);
			return state;
		}

		private static State GroupParser(char c, State state)
		{
			switch (c)
			{
				case '{':
					state.Depth++;
					break;
				case '}':
					state.Score += state.Depth;
					state.Depth--;
					break;
				case '<':
					state.Process = GarbageParser;
					break;
			}
			return state;
		}

		private static State GarbageParser(char c, State state)
		{
			switch (c)
			{
				case '!':
					state.Process = Ignore;
					break;
				case '>':
					state.Process = GroupParser;
					break;
				default:
					state.GarbageCount++;
					break;
			}
			return state;
		}

		private static State Ignore(char c, State state)
		{
			state.Process = GarbageParser;
			return state;
		}

		private struct State
		{
			public Func<char, State, State> Process;
			public int Depth;
			public int Score;
			public int GarbageCount;
		}
    }
}
