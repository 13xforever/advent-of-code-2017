﻿using System;

namespace Day03
{
    public static class Spiral
    {
		public static int GetLatency(int num)
		{
			if (num == 1)
				return 0;

			var (layerNumber, sideLength, layerStartNumber) = GetMinMaxForLayerWithNumber(num);
			var offset = (num - layerStartNumber) % (sideLength - 1) + 1;
			var center = layerNumber;
			var distanceFromCenter = 0;
			if (offset < center)
				distanceFromCenter = center - offset;
			else if (offset > center)
				distanceFromCenter = offset - center;
			return layerNumber + distanceFromCenter;
		}

		public static int GetNumberNextTo(int num)
		{
			const int maxHalfSize = 32;
			var field = new int[maxHalfSize*2+1, maxHalfSize*2+1];
			var (x, y) = (0, 0);
			field[maxHalfSize, maxHalfSize] = 1;

			int GetSum(int _x, int _y)
			{
				_x = maxHalfSize + _x;
				_y = maxHalfSize + _y;
				var result = field[_x - 1, _y - 1] +
				             field[_x - 1, _y] +
				             field[_x - 1, _y + 1] +
				             field[_x,     _y - 1] +
				             field[_x,     _y + 1] +
				             field[_x + 1, _y - 1] +
				             field[_x + 1, _y] +
				             field[_x + 1, _y + 1];
				field[_x, _y] = result;
				return result;
			}

			for (var layer = 1; layer < maxHalfSize; layer++)
			{
				x = layer;
				for (y = -layer+1; y <= layer; y++)
				{
					var sum = GetSum(x, y);
					if (sum > num)
						return sum;
				}
				y = layer;
				for (x = layer - 1; x >= -layer; x--)
				{
					var sum = GetSum(x, y);
					if (sum > num)
						return sum;
				}
				x = -layer;
				for (y = layer-1; y >= - layer; y--)
				{
					var sum = GetSum(x, y);
					if (sum > num)
						return sum;
				}
				y = -layer;
				for (x = -layer+1; x <= layer; x++)
				{
					var sum = GetSum(x, y);
					if (sum > num)
						return sum;
				}
			}
			return -1;
		}

		private static (int layer, int sideLength, int min) GetMinMaxForLayerWithNumber(int num)
		{
			var n = 1;
			var previousSideLength = 1;
			var sideLength = 3;
			do
			{
				var min = previousSideLength * previousSideLength + 1;
				var max = sideLength * sideLength;
				if (num >= min && num <= max)
					return (n, sideLength, min);

				n++;
				previousSideLength += 2;
				sideLength += 2;
			} while (true);
		}
    }
}
