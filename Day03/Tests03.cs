﻿using NUnit.Framework;

namespace Day03
{
	[TestFixture]
	public class Tests03
	{
		[TestCase(1, ExpectedResult = 0)]
		[TestCase(12, ExpectedResult = 3)]
		[TestCase(23, ExpectedResult = 2)]
		[TestCase(26, ExpectedResult = 5)]
		[TestCase(48, ExpectedResult = 5)]
		[TestCase(1024, ExpectedResult = 31)]
		[TestCase(312051, ExpectedResult = 430)]
		public int Part1(int cell) => Spiral.GetLatency(cell);

		[TestCase(1, ExpectedResult = 2)]
		[TestCase(4, ExpectedResult = 5)]
		[TestCase(12, ExpectedResult = 23)]
		[TestCase(700, ExpectedResult = 747)]
		[TestCase(312051, ExpectedResult = 312453)]
		public int Part2(int num) => Spiral.GetNumberNextTo(num);
	}
}