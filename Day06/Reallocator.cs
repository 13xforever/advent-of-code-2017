﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day06
{
    public static class Reallocator
	{
		private static readonly char[] separator = {' ', '\t'};

		public static (int cycles, int loopLength) CountOps(string memoryMap)
		{
			var memMap = memoryMap.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray();
			var state = GetHash(memMap);
			var states = new HashSet<int>(); //note: this is a _really_ bad idea, never do this in real code
			var history = new List<int>();
			var n = 0;
			do
			{
				states.Add(state);
				history.Add(state);
				state = SimpleToTheRight(memMap);
				n++;
			} while (!states.Contains(state));

			var loopLength = history.Count - history.FindIndex(i => i==state);
			return (n, loopLength);
		}

		public static int SimpleToTheRight(int[] memMap)
		{
			var memBank = 0;
			for (var i = 1; i < memMap.Length; i++)
			{
				if (memMap[i] > memMap[memBank])
					memBank = i;
			}
			var count = memMap[memBank];
			memMap[memBank] = 0;
			for (var i = 0; i < count; i++)
				memMap[(memBank + i + 1) % memMap.Length]++;
			return GetHash(memMap);
		}

		private static int GetHash(int[] memMap)
		{
			var result = 0;
			foreach (var n in memMap)
				result = (result * 397) ^ n;
			return result;
		}
    }
}
