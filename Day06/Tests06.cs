﻿using NUnit.Framework;

namespace Day06
{
	[TestFixture]
	public class Tests06
	{
		[TestCase("0 2 7 0", ExpectedResult = 5)]
		[TestCase("11	11	13	7	0	15	5	5	4	4	1	1	7	1	15	11", ExpectedResult = 4074)]
		public int Part1(string memoryMap) => Reallocator.CountOps(memoryMap).cycles;

		[TestCase("0 2 7 0", ExpectedResult = 4)]
		[TestCase("11	11	13	7	0	15	5	5	4	4	1	1	7	1	15	11", ExpectedResult = 2793)]
		public int Part2(string memoryMap) => Reallocator.CountOps(memoryMap).loopLength;
	}
}