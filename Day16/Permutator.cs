﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Day16
{
	using InstructionCache = Dictionary<string, Action>;

	public static class Permutator
	{
		private static readonly char[] ValueSeparator = {',', ' ', '\t'};
		private static readonly char[] InstructionSeparator = {'/'};

		public static string Reorder(int lineLength, int iterations, string input)
		{
			var lineup = new Lineup(lineLength);
			var instructions = input.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries);
			var cache = new InstructionCache();

			void Spin(int count)
			{
				var result = lineup.Backup;
				var line = lineup.Line;
				Buffer.BlockCopy(line, 0, result, count * sizeof(char), (line.Length - count) * sizeof(char));
				Buffer.BlockCopy(line, (line.Length - count) * sizeof(char), result, 0, count * sizeof(char));
				lineup.Line = result;
				lineup.Backup = line;
			}
			void Exchange(int posA, int posB)
			{
				var tmp = lineup.Line[posA];
				lineup.Line[posA] = lineup.Line[posB];
				lineup.Line[posB] = tmp;
			}
			void Partner(char a, char b)
			{
				var (posA, posB) = lineup.Line.IndexOf(a, b);
				Exchange(posA, posB);
			}

			var results = new Dictionary<string, int>();
			for (var i = 0; i < iterations; i++)
			{ foreach (var ins in instructions)
				{
					if (!cache.TryGetValue(ins, out var func))
						switch (ins[0])
						{
							case 's':
								var count = int.Parse(ins.Substring(1));
								cache[ins] = func = () => Spin(count);
								break;
							case 'x':
								var pos = ins.SplitIns();
								var posA = pos[0];
								var posB = pos[1];
								cache[ins] = func = () => Exchange(posA, posB);
								break;
							case 'p':
								var a = ins[1];
								var b = ins[3];
								cache[ins] = func = () => Partner(a, b);
								break;
							default:
								throw new FormatException("Unknown instruction " + ins);
						}
					func();
				}
				var l = string.Concat(lineup.Line);
				if (results.TryGetValue(l, out var idx))
				{
					var cycleLength = i - idx;
					var remaining = iterations - i - 1;
					var remainder = remaining % cycleLength;
					var resultIdx = idx + remainder;
					return results.First(kvp => kvp.Value == resultIdx).Key;
				}
				if (results.Count < int.MaxValue)
					results[l] = i;
			}
			return string.Concat(lineup.Line);
		}

		private static (int posA, int posB) IndexOf<T>(this IEnumerable<T> seq, T valueA, T valueB)
		{
			var idx = 0;
			int posA = -1, posB = -1;
			foreach (var e in seq)
			{
				if (valueA.Equals(e))
				{
					posA = idx;
					if (posA != -1 && posB != -1)
						return (posA, posB);
				}
				else if (valueB.Equals(e))
				{
					posB = idx;
					if (posA != -1 && posB != -1)
						return (posA, posB);
				}
				idx++;
			}
			return (posA, posB);
		}

		private static int[] SplitIns(this string ins)
		{
			return ins.Substring(1).Split(InstructionSeparator).Select(int.Parse).ToArray();
		}

		private struct Lineup
		{
			public Lineup(int length)
			{
				Line = Enumerable.Range(0, length).Select(n => (char)('a' + n)).ToArray();
				Backup = new char[length];
			}

			public char[] Line;
			public char[] Backup;
		}
	}
}
