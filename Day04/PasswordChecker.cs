﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day04
{
    public static class PasswordChecker
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};
		private static readonly char[] WordSeparator = {'\t', ' '};

		public static int GetValidCount(string passphraseList, Func<string, bool> policy)
		{
			return passphraseList.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries).Where(policy).Count();
		}

		public static bool AllUnique(string passphrase)
		{
			if (string.IsNullOrEmpty(passphrase))
				return false;

			var words = passphrase.Split(WordSeparator, StringSplitOptions.RemoveEmptyEntries);
			var uniqueWords = new HashSet<string>(words);
			return words.Length == uniqueWords.Count;
		}

		public static bool NoAnagrams(string passphrase)
		{
			if (string.IsNullOrEmpty(passphrase))
				return false;

			var words = passphrase.Split(WordSeparator, StringSplitOptions.RemoveEmptyEntries);
			var sortedLetters = words.Select(w => new string(w.ToCharArray().OrderBy(c => c).ToArray())).ToList();
			var uniqueWords = new HashSet<string>(sortedLetters);
			return words.Length == uniqueWords.Count;
		}
	}
}
