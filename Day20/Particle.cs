﻿using System.Diagnostics;

namespace Day20
{
	public struct Particle
	{
		public int Id;
		public Coord Pos;
		public Coord Velocity;
		public Coord Acceleration;

		public int Distance => Pos.Distance;
		public int DistanceTo(Particle other) => Pos.DistanceTo(other.Pos);

		public static bool operator <=(Particle a, Particle b) => a.Distance <= b.Distance;
		public static bool operator >=(Particle a, Particle b) => a.Distance >= b.Distance;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			return obj is Particle particle && Equals(particle);
		}

		public bool Equals(Particle other)
		{
			return Id == other.Id && Pos.Equals(other.Pos) && Velocity.Equals(other.Velocity) && Acceleration.Equals(other.Acceleration);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = Id;
				hashCode = (hashCode * 397) ^ Pos.GetHashCode();
				hashCode = (hashCode * 397) ^ Velocity.GetHashCode();
				hashCode = (hashCode * 397) ^ Acceleration.GetHashCode();
				return hashCode;
			}
		}

		public static bool operator ==(Particle left, Particle right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Particle left, Particle right)
		{
			return !left.Equals(right);
		}

		public override string ToString()
		{
			return $"{Id}: p={Pos} v={Velocity} a={Acceleration}";
		}
	}
}