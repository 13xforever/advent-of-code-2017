﻿using System;
using System.Diagnostics;

namespace Day20
{
	public struct Coord
	{
		public int X;
		public int Y;
		public int Z;

		public int Distance => Math.Abs(X) + Math.Abs(Y) + Math.Abs(Z);
		public int DistanceTo(Coord other) => Math.Abs(X - other.X) + Math.Abs(Y - other.Y) + Math.Abs(Z - other.Z);

		public static Coord operator +(Coord a, Coord b) => new Coord {X = a.X + b.X, Y = a.Y + b.Y, Z = a.Z + b.Z};
		public static bool operator <(Coord a, Coord b) => a.Distance < b.Distance;
		public static bool operator >(Coord a, Coord b) => a.Distance > b.Distance;

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj))
				return false;
			return obj is Coord coord && Equals(coord);
		}

		public bool Equals(Coord other)
		{
			return X == other.X && Y == other.Y && Z == other.Z;
		}

		public override int GetHashCode()
		{
			unchecked
			{
				var hashCode = X;
				hashCode = (hashCode * 397) ^ Y;
				hashCode = (hashCode * 397) ^ Z;
				return hashCode;
			}
		}

		public static bool operator ==(Coord left, Coord right)
		{
			return left.Equals(right);
		}

		public static bool operator !=(Coord left, Coord right)
		{
			return !left.Equals(right);
		}

		public override string ToString()
		{
			return $"<{X}, {Y}, {Z}>";
		}
	}
}