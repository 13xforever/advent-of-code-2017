﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day20
{
	public static class ParticleSimulator
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};
		private static readonly char[] ValueSeparator = {' ', '\t', '=', '<', '>', ',', 'p', 'v', 'a'};

		public static int GetClosestParticle(string input)
		{
			var particles = Parse(input);
			var state = new State {front = particles, back = new List<Particle>(particles.Count)};
			List<Particle> activeParticles;
			do
			{
				activeParticles = Tick(state).GetActiveParticles();
				state.front = activeParticles;
				state.back = new List<Particle>(activeParticles.Count);
			} while (activeParticles.Count > 1);
			return activeParticles[0].Id;
		}

		public static int GetParticleCountAfterCollisions(string input)
		{
			var particles = Parse(input);
			var state = new State { front = particles, back = new List<Particle>(particles.Count) };
			var result = new HashSet<int>(particles.Select(p => p.Id));
			HashSet<int> probableCollisions;
			do
			{
				var collisions = Tick(state).GetCollisions();
				if (collisions.Count > 0)
				{
					state.front = state.front.Where(p => !collisions.Contains(p.Id)).ToList();
					state.back = state.back.Where(p => !collisions.Contains(p.Id)).ToList();
					result.ExceptWith(collisions);
				}
				probableCollisions = state.GetProbableCollisions();
				if (probableCollisions.Count < state.front.Count)
					state.front = state.front.Where(p => probableCollisions.Contains(p.Id)).ToList();
			} while (probableCollisions.Count > 0);
			return result.Count;
		}

		private static State Tick(State state)
		{
			var front = state.front;
			state.back.Clear();
			foreach (var fp in front)
			{
				var p = fp;
				p.Velocity += p.Acceleration;
				p.Pos += p.Velocity;
				state.back.Add(p);
			}
			state.front = state.back;
			state.back = front;
			return state;
		}

		private static List<Particle> Parse(string input)
		{
			return input.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries)
				.Select(l => (l:l, p:l.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray()))
				.Select((p, idx) => Parse(p.l, idx, p.p))
				.ToList();
		}

		private static Particle Parse(string line, int id, int[] coords)
		{
			if (coords.Length != 9)
				throw new FormatException("Can't parse line " + string.Join(" ", line));

			return new Particle
			{
				Id = id,
				Pos = new Coord { X = coords[0], Y = coords[1], Z = coords[2]},
				Velocity = new Coord { X = coords[3], Y = coords[4], Z = coords[5]},
				Acceleration = new Coord { X = coords[6], Y = coords[7], Z = coords[8]},
			};
		}

		private class State
		{
			public List<Particle> front;
			public List<Particle> back;

			public List<Particle> GetActiveParticles()
			{
				var result = new List<Particle>();
				for (var i = 0; i < front.Count; i++)
				{
					var op = back[i];
					var np = front[i];
					if (np >= op && np.Velocity > op.Velocity)
						continue;

					result.Add(np);
				}
				return result;
			}

			public HashSet<int> GetCollisions()
			{
				var result = new HashSet<int>();
				for (var i = 0; i < front.Count - 1; i++)
				for (var j = i + 1; j < front.Count; j++)
				{
					if (front[i].Pos == front[j].Pos)
					{
						result.Add(front[i].Id);
						result.Add(front[j].Id);
					}
				}
				return result;
			}

			public HashSet<int> GetProbableCollisions()
			{
				var result = new HashSet<int>();
				for (var i = 0; i < front.Count - 1; i++)
				for (var j = i + 1; j < front.Count; j++)
				{
					var oldDist = back[i].DistanceTo(back[j]);
					var oldVelocity = back[i].Velocity.DistanceTo(back[j].Velocity);
					var newDist = front[i].DistanceTo(front[j]);
					var newVelocity = front[i].Velocity.DistanceTo(front[j].Velocity);
					if (newDist > oldDist && newVelocity > oldVelocity)
						continue;

					result.Add(front[i].Id);
					result.Add(front[j].Id);
				}
				return result;
			}
		}
	}
}