﻿using NUnit.Framework;

namespace Day22
{
	[TestFixture]
	public class Tests22
	{
		[TestCase(TestInput, 0, ExpectedResult = 2)]
		[TestCase(TestInput, 1, ExpectedResult = 3)]
		[TestCase(TestInput, 2, ExpectedResult = 2)]
		[TestCase(TestInput, 6, ExpectedResult = 6)]
		[TestCase(TestInput, 7, ExpectedResult = 5)]
		[TestCase(TestInput, 70, ExpectedResult = 14)]
		public int FinalInfectionCount(string input, int iterations) => Virus.GetFinalInfectionCount(input, iterations, Virus.Binary);

		[TestCase(TestInput, 70, ExpectedResult = 41)]
		[TestCase(TestInput, 10000, ExpectedResult = 5587)]
		[TestCase(RealInput, 10000, ExpectedResult = 5450)]
		public int Part1(string input, int iterations) => Virus.GetTotalInfectionCount(input, iterations, Virus.Binary);

		[TestCase(TestInput, 100, ExpectedResult = 26)]
		[TestCase(TestInput, 10000000, ExpectedResult = 2511944)]
		[TestCase(RealInput, 10000000, ExpectedResult = 2511957)]
		public int Part2(string input, int iterations) => Virus.GetTotalInfectionCount(input, iterations, Virus.Gradual);

		private const string TestInput = @"
..#
#..
...
";

		private const string RealInput = @"
#.....##.####.#.#########
.###..#..#..####.##....#.
..#########...###...####.
.##.#.##..#.#..#.#....###
...##....###..#.#..#.###.
###..#...######.####.#.#.
#..###..###..###.###.##..
.#.#.###.#.#...####..#...
##........##.####..##...#
.#.##..#.#....##.##.##..#
###......#..##.####.###.#
....#..###..#######.#...#
#####.....#.##.#..#..####
.#.###.#.###..##.#..####.
..#..##.###...#######....
.#.##.#.#.#.#...###.#.#..
##.###.#.#.###.#......#..
###..##.#...#....#..####.
.#.#.....#..#....##..#..#
#####.#.##..#...##..#....
##..#.#.#.####.#.##...##.
..#..#.#.####...#........
###.###.##.#..#.##.....#.
.##..##.##...#..#..#.#..#
#...####.#.##...#..#.#.##
";
	}
}
