﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day22
{
	using Field = Dictionary<(int x, int y), NodeStatus>;

	public static class Virus
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};

		public static int GetTotalInfectionCount(string input, int iterations, Func<NodeStatus, NodeStatus> nodeStatusCycle) => Simulate(input, iterations, nodeStatusCycle).infectionCount;
		public static int GetFinalInfectionCount(string input, int iterations, Func<NodeStatus, NodeStatus> nodeStatusCycle) => Simulate(input, iterations, nodeStatusCycle).field.Values.Count(s => s == NodeStatus.Infected);

		public static NodeStatus Binary(NodeStatus status) => status == NodeStatus.Normal ? NodeStatus.Infected : NodeStatus.Normal;

		public static NodeStatus Gradual(NodeStatus status)
		{
			switch (status)
			{
				case NodeStatus.Normal: return NodeStatus.Weak;
				case NodeStatus.Weak: return NodeStatus.Infected;
				case NodeStatus.Infected: return NodeStatus.Recovering;
				case NodeStatus.Recovering: return NodeStatus.Normal;
				default:
					throw new ArgumentOutOfRangeException(nameof(status), status, null);
			}
		}

		private static State Simulate(string input, int iterations, Func<NodeStatus, NodeStatus> nodeStatusCycle)
		{
			var field = Parse(input);
			var state = new State(field);
			for (var i = 0; i < iterations; i++)
				state = Burst(state, nodeStatusCycle);
			return state;
		}

		private static State Burst(State state, Func<NodeStatus, NodeStatus> nodeStatusCycle)
		{
			state.field.TryGetValue(state.coord, out var nodeStatus);
			switch (nodeStatus)
			{
				case NodeStatus.Normal:
					state.mv = TurnLeft(state.mv);
					break;
				case NodeStatus.Infected:
					state.mv = TurnRight(state.mv);
					break;
				case NodeStatus.Recovering:
					state.mv = (-state.mv.x, -state.mv.y);
					break;
			}
			nodeStatus = nodeStatusCycle(nodeStatus);
			if (nodeStatus == NodeStatus.Normal)
				state.field.Remove(state.coord);
			else
			{
				if (nodeStatus == NodeStatus.Infected)
					state.infectionCount++;
				state.field[state.coord] = nodeStatus;
			}
			state.coord = state.coord.Add(state.mv);
			return state;
		}

		private static (int x, int y) TurnLeft((int x, int y) mv)
		{
			if (mv.y == -1) return (-1, 0);
			if (mv.x == -1) return (0, 1);
			if (mv.y == 1) return (1, 0);
			if (mv.x == 1) return (0, -1);
			throw new InvalidOperationException($"Invalid motion vector ({mv.x}, {mv.y})");
		}

		private static (int x, int y) TurnRight((int x, int y) mv)
		{
			if (mv.y == -1) return (1, 0);
			if (mv.x == 1) return (0, 1);
			if (mv.y == 1) return (-1, 0);
			if (mv.x == -1) return (0, -1);
			throw new InvalidOperationException($"Invalid motion vector ({mv.x}, {mv.y})");
		}

		private static (int x, int y) Add(this (int x, int y) a, (int x, int y) b) => (a.x + b.x, a.y + b.y);

		private static Field Parse(string input)
		{
			var fieldSrc = input.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries)
				.Select(l => l.ToCharArray())
				.ToArray();
			var height = fieldSrc.Length;
			var width = fieldSrc[0].Length;
			if (height % 2 == 0 || width % 2 == 0)
				throw new InvalidOperationException($"Need to figure out where the (0, 0) is for a field of {width}x{height}");

			var result = new Field();
			var offsetY = -height / 2;
			var offsetX = -width / 2;
			for (var y = 0; y < height; y++)
				for (var x = 0; x < width; x++)
					if (fieldSrc[y][x] == '#')
						result[(x + offsetX, y + offsetY)] = NodeStatus.Infected;
			return result;
		}

		private struct State
		{
			public State(Field field)
			{
				coord = (0, 0);
				mv = (0, -1);
				this.field = field;
				infectionCount = 0;
			}

			public (int x, int y) mv;
			public (int x, int y) coord;
			public readonly Field field;
			public int infectionCount;
		}
	}

	public enum NodeStatus
	{
		Normal,
		Weak,
		Infected,
		Recovering,
	}
}