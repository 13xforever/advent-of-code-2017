﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day19
{
	public static class PacketTracer
	{
		private static readonly char[] LineSeparator = {'\r', '\n'};

		public static string GetPath(string input)
		{
			return string.Concat(Traverse(input).Path);
		}
		public static int CountSteps(string input)
		{
			return Traverse(input).Steps;
		}

		private static State Traverse(string input)
		{
			var maze = input.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries)
				.Select(l => l.ToCharArray())
				.ToArray();
			var state = new State(maze);
			do
			{
				state = state.Move(state);
			} while (state.IsActive);
			return state;
		}

		private class State
		{
			public State(char[][] maze) => this.maze = maze;

			public (int y, int x) Pos = (0, 0);
			public (int y, int x) Mv = (1, 0);
			public readonly List<char> Path = new List<char>();
			public bool IsActive = true;
			public Func<State, State> Move = FindEntryPoint;
			public int Steps { get; private set; } = 0;
			private readonly char[][] maze;

			private static State FindEntryPoint(State state)
			{
				state.Pos = (0, state.maze[0].IndexOf(c => c != ' '));
				state.Move = MoveNext;
				return state;
			}

			private static State MoveNext(State state)
			{
				var (y, x) = state.Pos;
				var (my, mx) = state.Mv;
				var c = state.maze[y][x];
				switch (c)
				{
					case '+':
					{
						if (my == 0)
						{
							if (y - 1 > -1 && state.maze[y - 1][x] != ' ')
							{
								state.Pos = (y - 1, x);
								state.Mv = (-1, 0);
							}
							else if (y + 1 < state.maze.Length && state.maze[y + 1][x] != ' ')
							{
								state.Pos = (y + 1, x);
								state.Mv = (1, 0);
							}
							else throw new InvalidOperationException($"Must turn up or down, but can't! (x:{x}, y:{y})");
						}
						else if (mx == 0)
						{
							if (x - 1 > -1 && state.maze[y][x - 1] != ' ')
							{
								state.Pos = (y, x - 1);
								state.Mv = (0, -1);
							}
							else if (x + 1 < state.maze[y].Length && state.maze[y][x + 1] != ' ')
							{
								state.Pos = (y, x + 1);
								state.Mv = (0, 1);
							}
							else throw new InvalidOperationException($"Must turn left or right, but can't! (x:{x}, y:{y})");
						}
						else throw new InvalidOperationException($"Invalid motion vector (mv x:{mx}, mv y:{my}");
						state.Steps++;
						return state;
					}
					case ' ':
					{
						state.IsActive = false;
						state.Move = Nop;
						return state;
					}
					default:
					{
						if (c != '-' && c != '|')
							state.Path.Add(c);
						var ny = state.Pos.y + state.Mv.y;
						var nx = state.Pos.x + state.Mv.x;
						if (ny < 0 || ny >= state.maze.Length || nx < 0 || nx >= state.maze[y].Length)
							throw new InvalidOperationException($"Moved outside of the maze (x:{nx}, y:{ny})");

						state.Pos = (ny, nx);
						state.Steps++;
						return state;
					}
				}
			}

			private static State Nop(State state) => state;
		}

		private static int IndexOf<T>(this IEnumerable<T> seq, Func<T, bool> selector)
		{
			var result = -1;
			foreach (var i in seq)
			{
				result++;
				if (selector(i))
					return result;
			}
			return result;
		}
	}
}