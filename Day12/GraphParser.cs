﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day12
{
	using Graph = Dictionary<int, HashSet<int>>;

    public static class GraphParser
    {
		private static readonly char[] LineSeparator = { '\r', '\n' };
		private static readonly char[] ValueSeparator = {',', ' ', '\t'};

		public static int GetConnectedNodeCount(string input)
		{
			return GetIslandNodes(Parse(input), 0).Count;
		}

		public static int GetIslandCount(string input)
		{
			var graph = Parse(input);
			var allNodes = new HashSet<int>(graph.Keys);
			var islandCount = 0;
			while (allNodes.Count > 0)
			{
				var islandNodes = GetIslandNodes(graph, allNodes.First());
				allNodes.ExceptWith(islandNodes);
				islandCount++;
			}
			return islandCount;
		}

		private static Graph Parse(string input)
		{
			var lines = input.Split(LineSeparator, StringSplitOptions.RemoveEmptyEntries).Select(l => l.Split(ValueSeparator, StringSplitOptions.RemoveEmptyEntries));
			var result = new Graph();
			foreach (var line in lines)
			{
				if (line.Length < 2 || line[1] != "<->")
					throw new FormatException("Invalid line: " + string.Join(" ", line));

				var id = int.Parse(line[0]);
				var connectedIds = line.Skip(2).Select(int.Parse).Where(cid => cid != id).ToList();
				if (!result.ContainsKey(id))
					result[id] = new HashSet<int>();
				foreach (var cid in connectedIds)
				{
					result[id].Add(cid);
					if (!result.TryGetValue(cid, out var connections))
						result[cid] = connections = new HashSet<int>();
					connections.Add(cid);
				}
			}
			return result;
		}

		private static HashSet<int> GetIslandNodes(Graph graph, int startId)
		{
			var checkedNodes = new HashSet<int>{ startId };
			var connectedNodes = new HashSet<int>(graph[startId]);
			do
			{
				var newFront = new HashSet<int>();
				foreach (var n in connectedNodes)
				{
					if (checkedNodes.Add(n))
						foreach (var cid in graph[n])
							if (cid != n && !connectedNodes.Contains(cid))
								newFront.Add(cid);
				}
				connectedNodes = newFront;
			} while (connectedNodes.Count > 0);
			return checkedNodes;
		}
    }
}
