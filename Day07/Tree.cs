﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Day07
{
	public class Tree
	{
		[DebuggerDisplay("{Name} ({Weight}) => {Value}")]
		public class Node
		{
			public string Name { get; }
			public int Weight { get; }
			public int TotalWeight { get; }
			public bool IsBalanced { get; }
			public List<Node> Children { get; }
			public Dictionary<string, Node> AllSubnodes { get; }

			public Node(string name, int weight, List<Node> children = null)
			{
				Name = name;
				Weight = weight;
				Children = children ?? new List<Node>(1);
				AllSubnodes = Children.ToDictionary(n => n.Name, n => n);
				foreach (var child in Children)
				foreach (var subnode in child.AllSubnodes)
					AllSubnodes.Add(subnode.Key, subnode.Value);
				TotalWeight = Weight + Children.Select(n => n.TotalWeight).Sum();
				IsBalanced = Children.Count == 0 || Children.All(n => n.TotalWeight == Children[0].TotalWeight);
			}

			private string Value => string.Join(", ", Children.Select(n => n.Name)) + " | " + TotalWeight;
		}

		public Node Root { get; }

		private Tree(Node root)
		{
			Root = root;
		}

		public static Tree Build(List<ProtoNode> nodes)
		{
			var protonodeMap = nodes.ToDictionary(n => n.Name, n => n);
			var nodeMap = new Dictionary<string, Node>(protonodeMap.Count);

			Node BuildSubtree(string name)
			{
				var protonode = protonodeMap[name];
				if (protonode.Children.Count == 0)
					return new Node(protonode.Name, protonode.Weight);

				var children = new List<Node>(protonode.Children.Count);
				foreach (var child in protonode.Children)
				{
					if (!nodeMap.ContainsKey(child))
						nodeMap.Add(child, BuildSubtree(child));
					children.Add(nodeMap[child]);
				}
				return new Node(protonode.Name, protonode.Weight, children);
			}

			var root = BuildSubtree(nodes[0].Name);
			foreach (var protonode in nodes)
			{
				if (!nodeMap.ContainsKey(protonode.Name))
				{
					var newRoot = BuildSubtree(protonode.Name);
					if (newRoot.AllSubnodes.Count > root.AllSubnodes.Count)
						root = newRoot;
				}
			}
			return new Tree(root);
		}
	}
}