﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Day07
{
	[DebuggerDisplay("{Name} ({Weight}) => {Value}")]
	public class ProtoNode
	{
		private static readonly char[] valueSeparator = { ' ', '\t', ',' };

		public string Name { get; }
		public int Weight { get; }
		public List<string> Children { get; }

		public ProtoNode(string name, int weight, List<string> children = null)
		{
			Name = name;
			Weight = weight;
			Children = children ?? new List<string>(0);
		}

		public static ProtoNode Parse(string nodeDesc)
		{
			var parts = nodeDesc.Trim().Split(valueSeparator, StringSplitOptions.RemoveEmptyEntries);
			if (parts.Length < 2)
				throw new FormatException($"Can't parse node <{nodeDesc}>");

			var name = parts[0];
			if (!(parts[1].StartsWith("(") && parts[1].EndsWith(")")))
				throw new FormatException($"'{parts[1]}' is not a valid weight");

			var weight = int.Parse(parts[1].Trim('(', ')'));
			if (parts.Length == 2)
				return new ProtoNode(name, weight);

			if (parts.Length == 3)
				throw new FormatException($"Invalid node format: {nodeDesc}");

			return new ProtoNode(name, weight, parts.Skip(3).ToList());
		}

		private string Value => string.Join(", ", Children);
	}
}