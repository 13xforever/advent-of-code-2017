﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Day07
{
    public static class Balancer
	{
		private static readonly char[] lineSeparator = {'\r', '\n'};

		public static string GetRootName(string input)
		{
			return Tree.Build(ReadNodes(input)).Root.Name;
		}

		public static int GetCorrectedNodeWeight(string input)
		{
			var root = Tree.Build(ReadNodes(input)).Root;
			var unbalancedNode = root.AllSubnodes.Values.FirstOrDefault(n => !n.IsBalanced && n.Children.All(sn => sn.IsBalanced)) ?? root;
			if (unbalancedNode.IsBalanced)
				return 0;

			var whiteNode = unbalancedNode.Children[0];
			var blackNode = unbalancedNode.Children.First(n => n.TotalWeight != whiteNode.TotalWeight);
			var blackNodeCount = unbalancedNode.Children.Count(n => n.TotalWeight == blackNode.TotalWeight);
			if (blackNodeCount > 1)
			{
				var tmp = whiteNode;
				whiteNode = blackNode;
				blackNode = tmp;
			}
			return blackNode.Weight - (blackNode.TotalWeight - whiteNode.TotalWeight);
		}

		private static List<ProtoNode> ReadNodes(string input)
		{
			return input.Split(lineSeparator, StringSplitOptions.RemoveEmptyEntries).Select(ProtoNode.Parse).ToList();
		}
    }
}
