﻿using NUnit.Framework;

namespace Day10
{
	[TestFixture]
	public class Tests10
	{
		[TestCase("3, 4, 1, 5", ExpectedResult = 12)]
		[TestCase("230,1,2,221,97,252,168,169,57,99,0,254,181,255,235,167", ExpectedResult = 8)]
		public int Part1(string input) => KnotHash.GetProduct(input, 5);

		[TestCase("", ExpectedResult = "a2582a3a0e66e6e86e3812dcb672a272")]
		[TestCase("AoC 2017", ExpectedResult = "33efeb34ea91902bb2f59c9920caa6cd")]
		[TestCase("1,2,3", ExpectedResult = "3efbe78a8d82f29979031a4aa0b16a9d")]
		[TestCase("1,2,4", ExpectedResult = "63960835bcdc130f0b66d7ff4f6a5a8e")]
		[TestCase("230,1,2,221,97,252,168,169,57,99,0,254,181,255,235,167", ExpectedResult = "0c2f794b2eb555f7830766bf8fb65a16")]
		public string Part2(string input) => KnotHash.GetHash(input);
	}
}