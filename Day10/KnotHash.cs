﻿using System;
using System.Linq;
using System.Text;

namespace Day10
{
	public static class KnotHash
	{
		private static readonly char[] separator = {',', '\t', ' '};

		public static int GetProduct(string input, int dataSize = 256)
		{
			var data = Enumerable.Range(0, dataSize).Select(i => (byte)i).ToArray();
			var lengths = input.Split(separator, StringSplitOptions.RemoveEmptyEntries).Select(byte.Parse).ToArray();
			var result = Compute(data, lengths).data;
			return result[0] * result[1];
		}

		public static string GetHash(string input)
		{
			var result = GetHashBytes(input);
			var sb = new StringBuilder();
			foreach (var b in result)
				sb.Append(b.ToString("x2"));
			return sb.ToString();
		}

		public static byte[] GetHashBytes(string input)
		{
			var data = Enumerable.Range(0, 256).Select(i => (byte)i).ToArray();
			var lengths = input.Select(c => (byte)c).Concat(new byte[] {17, 31, 73, 47, 23}).ToArray();
			int position = 0;
			int skip = 0;
			for (var r = 0; r < 64; r++)
				(data, position, skip) = Compute(data, lengths, position, skip);
			return GetDenseHash(data);
		}

		private static (byte[] data, int position, int skip) Compute(byte[] data, byte[] lengths, int position = 0, int skip = 0)
		{
			foreach (var reverseLength in lengths)
			{
				for (int i = position, j = position + reverseLength - 1; i < (position + reverseLength / 2); i++, j--)
				{
					var s = i % data.Length;
					var e = j % data.Length;
					var tmp = data[s];
					data[s] = data[e];
					data[e] = tmp;
				}
				position += reverseLength + skip;
				skip++;
			}
			return (data, position % data.Length, skip % data.Length);
		}

		private static byte[] GetDenseHash(byte[] sparseHash)
		{
			var result = new byte[sparseHash.Length / 16];
			for (var i = 0; i < result.Length; i++)
			for (var j = i * 16; j < i * 16 + 16; j++)
				result[i] ^= sparseHash[j];
			return result;
		}
	}
}
