﻿using System;
using System.Collections.Generic;
using Day10;

namespace Day14
{
	using Front = HashSet<(int x, int y)>;

	public static class Disk
    {
		public static int GetOccupiedBlockCount(string seed)
		{
			var disk = GenerateDisk(seed);
			var result = 0;
			foreach (var row in disk)
				for (var idx = 0; idx < row.Length; idx += sizeof(ulong))
					result += Popcnt(BitConverter.ToUInt64(row, idx));
			return result;
		}

		public static int GetRegionCount(string seed)
		{
			var disk = GenerateDisk(seed);
			var map = new int[128, 128];
			for (var y = 0; y < 128; y++)
			for (var x = 0; x < 128; x++)
			{
				map[x, y] = (disk[y][x / 8] & (1 << (7 - (x % 8)))) == 0 ? 0 : -1;
			}
			return CountRegions(map);
		}

		private static byte[][] GenerateDisk(string seed)
		{
			var result = new byte[128][];
			for (var i = 0; i < 128; i++)
				result[i] = KnotHash.GetHashBytes($"{seed}-{i}");
			return result;
		}

		//this is where System.Numerics.Intrinsics would've been great to have https://github.com/dotnet/corefx/issues/2209
		private static byte Popcnt(ulong x)
		{
			x -= (x >> 1) & 0x5555555555555555UL;
			x = ((x >> 2) & 0x3333333333333333UL) + (x & 0x3333333333333333UL);
			x = ((x >> 4) + x) & 0x0F0F0F0F0F0F0F0FUL;
			x *= 0x0101010101010101UL;
			return (byte)(x >> (64 - 8));
		}

		private static int CountRegions(int[,] map)
		{
			var i = 0;
			for (var y = 0; y < 128; y++)
			for (var x = 0; x < 128; x++)
				if (map[x, y] == -1)
					Fill(map, x, y, ++i);
			return i;
		}

		private static void Fill(int[,] map, int startX, int startY, int id)
		{
			var front = new Front {(startX, startY)};
			do
			{
				var newFront = new Front();
				foreach (var (x, y) in front)
					if (map[x, y] == -1)
					{
						map[x, y] = id;
						if (x > 0) newFront.Add((x - 1, y));
						if (x < 127) newFront.Add((x + 1, y));
						if (y > 0) newFront.Add((x, y - 1));
						if (y < 127) newFront.Add((x, y + 1));
					}
				front = newFront;
			} while (front.Count > 0);
		}
    }
}
