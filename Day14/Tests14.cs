﻿using NUnit.Framework;

namespace Day14
{
	[TestFixture]
	public class Tests14
	{
		[TestCase("flqrgnkx", ExpectedResult = 8108)]
		[TestCase("oundnydw", ExpectedResult = 8106)]
		public int Part1(string input) => Disk.GetOccupiedBlockCount(input);

		[TestCase("flqrgnkx", ExpectedResult = 1242)]
		[TestCase("oundnydw", ExpectedResult = 1164)]
		public int Part2(string input) => Disk.GetRegionCount(input);
	}
}